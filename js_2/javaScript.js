function page_loaded() {
    let numberOfFilms
    do {
        numberOfFilms = prompt('Сколько фильмов вы уже посмотрели?', '')
    } while (numberOfFilms === null || numberOfFilms.length === 0 || isNaN(+numberOfFilms))

    const personalMovieDB = {
        count: numberOfFilms,
        movies: {},
        actors: {},
        genres: [],
        privat: false
    }
    let movie, rating
    for (let i = 0; i < personalMovieDB.count; i++) {
        do {
            movie = prompt('Один из последних просмотренных фильмов?', '')
        } while (movie === null || movie.length === 0 || movie.length > 50)

        do {
            rating = prompt('На сколько оцените его?', '')
        } while (rating === null || rating.length === 0)

        personalMovieDB.movies[movie] = rating
    }
    if (personalMovieDB.count < 10) console.log('Просмотрено довольно мало фильмов')
    else if (personalMovieDB.count >= 10 && personalMovieDB.count <= 30) console.log('Вы классический зритель')
    else if (personalMovieDB.count > 30) console.log('Вы киноман')
    else console.log('Произошла ошибка')

    /* Второй вариант цикла:
    i = 0
    while (i < personalMovieDB.count) {
        do {
            movie = prompt('Один из последних просмотренных фильмов?', '')
        } while (movie === null || movie.length === 0 || movie.length > 50)

        do {
            rating = prompt('На сколько оцените его?', '')
        } while (rating === null || rating.length === 0)

        personalMovieDB.movies[movie] = rating
        i = i + 1
    }

    Третий вариант цикла:
    i = 0
    do {
        if (i >= personalMovieDB.count) break
        do {
            movie = prompt('Один из последних просмотренных фильмов?', '')
        } while (movie === null || movie.length === 0 || movie.length > 50)

        do {
            rating = prompt('На сколько оцените его?', '')
        } while (rating === null || rating.length === 0)

        personalMovieDB.movies[movie] = rating
        i = i + 1
    } while (true)
    */
}

document.addEventListener('DOMContentLoaded', page_loaded)
